package br.com.ozeano.curso.api.bb.infra.service.file;

import org.springframework.stereotype.Service;

import br.com.caelum.stella.boleto.transformer.GeradorDeBoleto;
import br.com.ozeano.curso.api.bb.domain.model.Fatura;
import br.com.ozeano.curso.api.bb.domain.service.file.GeradorDeBoletoService;
import br.com.ozeano.curso.api.bb.infra.model.input.CobrancaInput;

@Service
public class PdfBoletoFileService implements GeradorDeBoletoService {

	@Override
	public byte[] gerar(Fatura fatura, CobrancaInput cobranca) {

		var boleto = criarBoleto(fatura, cobranca);

		var gerador = new GeradorDeBoleto(boleto);

		return gerador.geraPDF();
	}

}
